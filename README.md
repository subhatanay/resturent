
# PetPuja.com - E-Resturent Search

## Steps to Dump the CSV Content to DB

1. Login to Mysql Console
2. create database ResturentDB CHARACTER SET utf8 COLLATE utf8_general_ci;;
3. 

    CREATE TABLE  `resturent_details` (
    `id` int(11)  NOT NULL,
    `resturent_name` varchar(255)  NOT NULL,
    `cuisines` varchar(255) NOT NULL,
    `cost` int(11) NOT NULL,
    `currency` varchar(255)  NOT NULL,
    `hastable` int(1)  NOT NULL,
    `hasonline` int(1)  NOT NULL,
    `rate` varchar(50) NOT NULL,
    `rate_color` varchar(50)  NOT NULL,
    `rate_text` varchar(50)  NOT NULL,
    `votes` int(11)  NOT NULL,
    PRIMARY KEY (`id`)
   );

   CREATE  TABLE `user_details` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `userid` varchar (255)  NOT NULL UNIQUE,
    `password` varchar(10000)  NOT NULL,
    `name` varchar(255) NOT NULL,
    `email_address` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
   );

   CREATE TABLE `user_rating` (
    `user_id` int(11) NOT NULL,
    `resturent_id` int(11) NOT NULL,
    `rate` int(11) NOT NULL
   );

4. LOAD DATA LOCAL INFILE '<path/file.csv>' INTO TABLE resturent_details FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

5. CSV File will be uploaded to Relational DB

## Steps to run the application

1. Open src/main/resources/application.yml
2. Change the required port,username,password for your Mysql.
3. Server will run on 9090 port.
4. in command propt execute -> 
5. `gradle clean build.`
6. After build succcess -> 
7. `java -jar build\libs\petpuja-0.0.1-SNAPSHOT.jar`
8. Open a browser -> hit http://localhost:9090/home
9. It will open the UI for Checking the Youtube pages.

## Steps to run the application as Docker

1. Open src/main/resources/application.yml
2. Change the jdbc url to host ip.
3. Change the required port,username,password for your Mysql.
4. Open command prompt. 
5. `gradle clean buildDocker`
6. After success - 
7 docker run -it -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=subhajit123 mysql:5.7.17
7. `docker run -it -d -p 9090:9090 com.resturent/petpuja:0.0.1-SNAPSHOT`
8. Open a browser -> hit http://localhost:9090/home
9. It will open the UI for Checking the Resturent page.

## Devoloped API's
Here is the POSTMAN Link for API's https://www.getpostman.com/collections/2c8a0a10b83ed7e00f7f

I have devoloped 5 API those are as follows (Usages are defined in POSTMAN Collection):
1. /resturents API will return list of resturent with all coumn filter , sort support and limit support. ByDefault, Limit is 50, Only 50 record can be viewable at once.
2. /signup API will create a user for the resturent to vote for the resturent.
3. /login API will login for the user with username and password. It will return a JWT token. which token will be further use for autenticating resources.
4. /resturent/vote API will update a vote/rate for a resturent per logined user. Its a authenticated API. need to pass JWT token for authenticating.
5. /userinfo API will fetch userinformation like username,email and votedResturents link. This is authenticated API.

## Suggestion
Due to time not able to add the Resturent Map Localtion in UI side/API side. But Since Resturent Location information is static content not will change too much. we can
store it in NoSQL db. So it can be store a JSON document format. As NoSQL is best for fetching content. This whole JSON can be add to the /resturent API. Please consider here.


## Limitation

1. UI does not support all kind of Filter. API supports that need to add fot UI too.
2. Need to add caching mechanism for no of time resturent has been searched.
3. May be API and UI have some bugs needs to be fixed.
4. Authentication mechanism can be done by a seperate microservice. 

## Technologies
1. Java
2. Spring Boot(Backend)
3.  Thymleaf(UI)
4. Docker (Deployment)







