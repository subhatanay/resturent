var dialog_register;
var dialog_login;
var pageOptions = {
    currentPage: 0,
    totalRecords: 0,
    limit: 20
};
var colors = ["light-blue", "#59a1d5", "orange", "green", "violet"];
var resturmentCard = '<div style="margin:10px" id="resturent_[resid]" class="demo-card-square mdl-card mdl-shadow--2dp mdl-cell--4-col"> <div class="mdl-card__title mdl-card--expand" style="background: bottom right 15% no-repeat [color_index] !important"> <h2 class="mdl-card__title-text">[resturent_name]</h2> </div><div class="mdl-card__supporting-text"> <p>Type : [resturent_type]</p> <p>Cost of Two [cost]</p></div><div class="mdl-card__menu"> <button onclick="vote(\'[RID]\',\'5\')" class="mdl-button mdl-js-button mdl-button--icon"> <i id="button_[RID1]" class="material-icons"> [liked_fav] </i> </button> </div><div class="mdl-card__actions mdl-card--border"> <p>Rating : [rate] </p></div></div>';
var cnt = 0;
var globalContext = {};

document.addEventListener("DOMContentLoaded", function () {
    dialog_register = initDialog('#registerDiv');
    dialog_register.querySelector('.close').addEventListener('click', function () {
        dialog_register.close();
    });
    dialog_register.querySelector('.submit').addEventListener('click', function () {
        registerNewAccount();
    });
    dialog_login = initDialog('#loginDiv');
    dialog_login.querySelector('.close').addEventListener('click', function () {
        dialog_login.close();
    });
    dialog_login.querySelector('.submit').addEventListener('click', function () {
        LoginAccount();
    });
    document.querySelector("#generateSearch").addEventListener("click", function () {
        search();
    });
    document.querySelector("#registerLink").addEventListener("click", function () {
        refreshRegisterTextbox();
        dialog_register.showModal();
    });
    document.querySelector("#loginLink").addEventListener("click", function () {
        refreshLoginTextbox();
        dialog_login.showModal();
    });
    document.querySelector("#logoutLink").addEventListener("click", function () {
        if (sessionStorage.getItem("token") != null) {
            sessionStorage.clear();
            $("#loginLink").show();
            $("#registerLink").show();
            $("#logoutLink").hide();
            window.location.reload();
        }
    });


    if (sessionStorage.getItem("token") != null) {
        $("#loginLink").hide();
        $("#registerLink").hide();
        $("#logoutLink").show();
    }

    fetchTopResturentInfos();
});

function initDialog(dialogId) {
    var dialog = document.querySelector(dialogId);
    if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }

    return dialog;
}
function refreshRegisterTextbox() {
    document.querySelector("#username").value = "";
    document.querySelector("#username").parentElement.classList.remove('is-invalid');
    document.querySelector("#username").parentElement.classList.remove('is-dirty');

    var password = document.querySelector("#password").value = "";
    document.querySelector("#password").parentElement.classList.remove('is-invalid');
    document.querySelector("#password").parentElement.classList.remove('is-dirty');

    var confirmpassword = document.querySelector("#cpassword").value = "";
    document.querySelector("#cpassword").parentElement.classList.remove('is-invalid');
    document.querySelector("#cpassword").parentElement.classList.remove('is-dirty');

    var orgname = document.querySelector("#orgname").value = "";
    document.querySelector("#orgname").parentElement.classList.remove('is-invalid');
    document.querySelector("#orgname").parentElement.classList.remove('is-dirty');

    var domain = document.querySelector("#orgdomain").value = "";
    document.querySelector("#orgdomain").parentElement.classList.remove('is-invalid');
    document.querySelector("#orgdomain").parentElement.classList.remove('is-dirty');


}
function refreshLoginTextbox() {
    document.querySelector("#username1").value = "";
    document.querySelector("#username1").parentElement.classList.remove('is-invalid');
    document.querySelector("#username1").parentElement.classList.remove('is-dirty');

    var password = document.querySelector("#password1").value = "";
    document.querySelector("#password1").parentElement.classList.remove('is-invalid');
    document.querySelector("#password1").parentElement.classList.remove('is-dirty');

}


function userInfo() {
    $.ajax({
        async: false,
        type: "GET",
        url: "/userinfo",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "JWT " + sessionStorage.getItem("token")
        },
        success: function (data) {
            console.log(data);
            globalContext = data;
            for (var i = 0; i < data.likedResturents.length; i++) {
                $("#resturent_" + data.likedResturents[i] + " .mdl-card__menu .material-icons").html("favorite");
            }
            if (sessionStorage.getItem("token") != null) {
                $("#loginLink").hide();
                $("#registerLink").hide();
            }
        },
        error: function (data) {
            if (data.status === 400) {
                toast(data.responseJSON.message);
            }
            if (data.status === 401) {
                if (sessionStorage.getItem("token") != null) {
                    $("#loginLink").show();
                    $("#registerLink").show();
                }
            }
            if (data.status === 404) {
                toast(data.responseJSON.message);
            }

            if (data.status === 500) {
                toast("Oops some error occured...");
            }

        }
    });
}

function vote(id, rate) {
    if (sessionStorage.getItem("token") == null) {
        $("#loginLink").click();
        toast("Please login before vote/rate");
        return;
    }

    var data = {
        resturentId: id,
        rate: rate
    };

    $.ajax({
        async: false,
        type: "POST",
        url: "/resturent/vote",
        data: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": 'JWT ' + sessionStorage.getItem("token")
        },
        success: function (data) {
            toast("Voted Successfully", "green");
            window.location.reload();
        },
        error: function (data) {
            toast(data.responseJSON.message);
            if (data.status === 401) {
                if (sessionStorage.getItem("token") != null) {
                    $("#loginLink").show();
                    $("#registerLink").show();
                }
            }

            if (data.status === 500) {
                toast("Oops some error occured...");
            }

        }
    });
}
function registerNewAccount() {
    var data = {
        userId: document.querySelector("#username").value,
        password: document.querySelector("#password").value,
        emailAddress: document.querySelector("#orgdomain").value,
        name: document.querySelector("#orgname").value
    };

    $.ajax({
        async: false,
        type: "POST",
        url: "/signup",
        data: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        success: function (data) {
            dialog_register.close();
            toast("Registation successfull. Please Login with you Creds", "green");
        },
        error: function (data) {
            if (data.status === 400) {
                toast(data.responseJSON.message);
            }
            if (data.status === 404) {
                toast(data.responseJSON.message);
            }

            if (data.status === 500) {
                toast("Oops some error occured...");
            }

        }
    });
}
function LoginAccount() {
    var data = {
        userId: document.querySelector("#username1").value,
        password: document.querySelector("#password1").value
    };

    $.ajax({
        async: false,
        type: "POST",
        url: "/login",
        data: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        success: function (data) {
            dialog_login.close();
            sessionStorage.setItem("token", data.token);

            toast("Login successfull", "green");
            window.location.reload();
        },
        error: function (data) {
            if (data.status === 400) {
                toast(data.responseJSON.message);
            }
            if (data.status === 401) {
                toast("Unauthorized");
            }
            if (data.status === 404) {
                toast(data.responseJSON.message);
            }

            if (data.status === 500) {
                toast("Oops some error occured...");
            }

        }
    });
}


function createResturentCard(options, cc, liked) {
    var htmlContent = resturmentCard;
    htmlContent = htmlContent.replace("[resturent_name]", options.resturentName);
    htmlContent = htmlContent.replace("[cost]", options.cost);
    htmlContent = htmlContent.replace("[rate]", options.rate);
    htmlContent = htmlContent.replace("[resid]", options.resturentId);
    htmlContent = htmlContent.replace("[resturent_type]", options.cuisines);
    htmlContent = htmlContent.replace("[color_index]", colors[cnt++ % colors.length]);
    htmlContent = htmlContent.replace("[RID]", options.resturentId);
    if (liked) {
        htmlContent = htmlContent.replace("[liked_fav]", "favorite");
    } else {
        htmlContent = htmlContent.replace("[liked_fav]", "favorite_border");
    }

    $("#" + cc).append(htmlContent);
}
function search(options, nextpage) {
    var serachField = document.querySelector("#searchField").value;
    var searchMethod = document.querySelector("#methodField").value;
    var searchText = document.querySelector("#searchText").value;

    if (serachField == -1) {
        return toast("Select a Search Field");
    }
    if (searchMethod == -1) {
        return toast("Select a Search Method");
    }

    if (options) {
        options["filter"] = searchText !== "" ? searchMethod + "(" + serachField + "," + encodeURIComponent(searchText) + ")" : "";
        searchResturents(options, nextpage);
    } else {
        var options = {
            "filter": searchText !== "" ? searchMethod + "(" + serachField + "," + encodeURIComponent(searchText) + ")" : "",
            "limit": 20
        };
        searchResturents(options);
    }
}
 

function searchResturents(options) {
    var url = "/resturents?";
    if (options) {
        for (var o in options) {
            url = url + o + "=" + options[o] + "&";
        }
        url = url.substring(0, url.length - 1);
    } else {
        url = "resturents?limit=20";
    }
    $.ajax({
        async: false,
        type: "GET",
        url: url,
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        success: function (data) {
            poulateGrid(data, "searchedResturentsBody");
            $("#searchedResturents").show();
            $("#topResturents").hide();
            setMetaInfo(data);
        },
        error: function (data) {
            if (data.status === 404) {
                toast("No resturents found");
            }

            if (data.status === 500) {
                toast("Oops some error occured...");
            }

        }
    });
}
function fetchTopResturentInfos(options, nextPage) {
    var url = "/resturents?filter=gt(rate,4.5)&orderby=rate,desc&limit=20";

    $.ajax({
        async: false,
        type: "GET",
        url: url,
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        success: function (data) {

            poulateGrid(data, "topResturentsBody");
            $("#searchedResturents").hide();
            $("#topResturents").show();
            setMetaInfo(data);
            var sessionToken = sessionStorage.getItem("token");
            if (sessionToken) {
                userInfo();
            }
        },
        error: function (data) {
            if (data.status === 404) {
                toast("No resturents found");
            }

            if (data.status === 500) {
                toast("Oops some error occured...");
            }

        }
    });
}
function setMetaInfo(data) {
    var meta = data["metaInfo"];
    pageOptions["totalRecords"] = parseInt(meta.total);
    pageOptions["noOfPage"] = Math.round(parseInt(meta.total) / parseInt(meta.limit));
    pageOptions["limit"] = meta.limit;
    pageOptions["offset"] = meta.offset;
}
function poulateGrid(resturents, cc) {
    $("#" + cc).html("");
    var results = resturents["results"];
    for (var i = 0; i < results.length; i++) {
        var opt = {
            resturentId: results[i].resturentId,
            resturentName: results[i].resturentName,
            cost: results[i].cost,
            rate: results[i].rate,
            cuisines: results[i].cuisines,
        };
        if (globalContext.likedResturents && globalContext.likedResturents.indexOf(results[i].resturentId) > -1) {
            createResturentCard(opt, cc, true);
        }
        createResturentCard(opt, cc);
    }
}

function toast(message, color) {
    var snackbarContainer = document.querySelector('#bam-toast');
    var data1 = {
        message: message,
        timeout: 3000
    };

    snackbarContainer.style.alignContent = "center";
    snackbarContainer.classList.remove('mdl-color--green-700');
    snackbarContainer.classList.remove('mdl-color--red-700');
    if (color) {
        snackbarContainer.classList.add('mdl-color--green-700');
    } else {
        snackbarContainer.classList.add('mdl-color--red-700');
    }
    snackbarContainer.MaterialSnackbar.showSnackbar(data1);

}


