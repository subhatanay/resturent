/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

/**
 *
 * @author subhajgh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultModel {
    private MetaData metaInfo;
    private List<ResturentModel> results;
    private String error;
    private String token;

    /**
     * @return the metaInfo
     */
    public MetaData getMetaInfo() {
        return metaInfo;
    }

    /**
     * @param metaInfo the metaInfo to set
     */
    public void setMetaInfo(MetaData metaInfo) {
        this.metaInfo = metaInfo;
    }

    /**
     * @return the results
     */
    public List<ResturentModel> getResults() {
        return results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<ResturentModel> results) {
        this.results = results;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
    
}
