/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.models;

/**
 *
 * @author subhajgh
 */
public class ResturentModel {
    private String resturentId;
    private String resturentName;
    private String cuisines;
    private String cost;
    private String currency;
    private String hasTable;
    private String hasOnline;
    private String rate;
    private String vote;

    /**
     * @return the resturentId
     */
    public String getResturentId() {
        return resturentId;
    }

    /**
     * @param resturentId the resturentId to set
     */
    public void setResturentId(String resturentId) {
        this.resturentId = resturentId;
    }

    /**
     * @return the resturentName
     */
    public String getResturentName() {
        return resturentName;
    }

    /**
     * @param resturentName the resturentName to set
     */
    public void setResturentName(String resturentName) {
        this.resturentName = resturentName;
    }

    /**
     * @return the cuisines
     */
    public String getCuisines() {
        return cuisines;
    }

    /**
     * @param cuisines the cuisines to set
     */
    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    /**
     * @return the cost
     */
    public String getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(String cost) {
        this.cost = cost;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the hasTable
     */
    public String getHasTable() {
        return hasTable;
    }

    /**
     * @param hasTable the hasTable to set
     */
    public void setHasTable(String hasTable) {
        this.hasTable = hasTable;
    }

    /**
     * @return the hasOnline
     */
    public String getHasOnline() {
        return hasOnline;
    }

    /**
     * @param hasOnline the hasOnline to set
     */
    public void setHasOnline(String hasOnline) {
        this.hasOnline = hasOnline;
    }

    /**
     * @return the rate
     */
    public String getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(String rate) {
        this.rate = rate;
    }

    /**
     * @return the vote
     */
    public String getVote() {
        return vote;
    }

    /**
     * @param vote the vote to set
     */
    public void setVote(String vote) {
        this.vote = vote;
    }
            
}
