package com.resturent.petpuja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetpujaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetpujaApplication.class, args);
	}

}
