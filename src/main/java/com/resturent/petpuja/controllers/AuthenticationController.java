/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.controllers;

import com.resturent.petpuja.dao.ResturentDao;
import com.resturent.petpuja.models.ResponseModel;
import com.resturent.petpuja.models.ResultModel;
import com.resturent.petpuja.models.SignUpModel;
import com.resturent.petpuja.services.AuthenticationService;
import com.resturent.petpuja.utils.Validator;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author subhajgh
 */
@RestController
public class AuthenticationController {

    Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    AuthenticationService authService;
    @Autowired
    ResturentDao restDao;
    

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public Object signup(@RequestBody SignUpModel signUp) {
        try {
            String error = Validator.validateSignUpModel(signUp, "signup");
            if ("".equals(error)) {
                if (authService.createUser(signUp)) {
                    return new ResponseEntity<>(new ResponseModel("200", "User Created Successfully"), HttpStatus.CREATED);
                }
                return new ResponseEntity<>(new ResponseModel("400", "User not Successfully"), HttpStatus.BAD_REQUEST);
            }
            logger.error("Error. " + error);
            return new ResponseEntity<>(new ResponseModel("400", error), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while creating user . " + e.getMessage());
            return new ResponseEntity<>(new ResponseModel("500", "Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Object loginUser(@RequestBody SignUpModel signUp) {
        try {
            String error = Validator.validateSignUpModel(signUp, "login");
            if ("".equals(error)) {
                String token = authService.verifyUser(signUp);
                if (!"".equals(token)) {
                    ResultModel res = new ResultModel();
                    res.setToken(token);
                    return new ResponseEntity<>(res, HttpStatus.OK);
                }
                return new ResponseEntity<>(new ResponseModel("401", "User unauthorized"), HttpStatus.UNAUTHORIZED);
            }
            logger.error("Error. " + error);
            return new ResponseEntity<>(new ResponseModel("400", error), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while creating user . " + e.getMessage());
            return new ResponseEntity<>(new ResponseModel("500", "Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @RequestMapping(value="/userinfo",method=RequestMethod.GET)
    public Object userinfo(HttpServletRequest request) {
        try {
            SignUpModel model = (SignUpModel) request.getServletContext().getAttribute("authModel");
            model.setLikedResturents(restDao.listOfVotedResturents(model.getId()));
            return new ResponseEntity<>(model,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while creating user . " + e.getMessage());
            return new ResponseEntity<>(new ResponseModel("500", "Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        
    }

}
