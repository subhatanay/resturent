/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.controllers;

import com.resturent.petpuja.models.ResponseModel;
import com.resturent.petpuja.models.ResturentModel;
import com.resturent.petpuja.models.ResultModel;
import com.resturent.petpuja.models.SignUpModel;
import com.resturent.petpuja.services.ResturentService;
import com.resturent.petpuja.utils.Validator;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author subhajgh
 */
@RestController
public class ResturentController {

    Logger logger = LoggerFactory.getLogger(ResturentController.class);
    @Autowired
    ResturentService resturentService;

    @RequestMapping(value = "/resturents", method = RequestMethod.GET)
    public Object searchResturents(@RequestParam(value = "filter", defaultValue = "") String filter,
            @RequestParam(value = "orderby", defaultValue = "resturent_name,desc") String orderBy,
            @RequestParam(value = "limit", defaultValue = "50") int limit,
            @RequestParam(value = "offset", defaultValue = "0") int offset) {
        try {
            ResultModel results = resturentService.findResturents(filter, limit, offset, orderBy);
            if (results.getError() != null) {
                return new ResponseEntity<>(new ResponseModel("400", results.getError()), HttpStatus.BAD_REQUEST);
            }
            if (results.getMetaInfo().getTotal() > 0) {
                return new ResponseEntity<>(results, HttpStatus.OK);
            } else {
                logger.error("No resturents found");
                return new ResponseEntity<>(new ResponseModel("404", "No resturents found"), HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error searching resturents . " + e.getMessage());
            return new ResponseEntity<>(new ResponseModel("500", "Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/resturent/vote", method = RequestMethod.POST)
    public Object voteForResturent(@RequestBody ResturentModel resturentModel, HttpServletRequest request) {
        try {
            SignUpModel model = (SignUpModel) request.getServletContext().getAttribute("authModel");
            if (model != null && model.getId() != null) {
                String error = Validator.validateResturentModelForVote(resturentModel);
                if ("".equals(error)) {
                    if (!resturentService.isUserAlreadyVoted(model.getId(), resturentModel)) {
                        boolean ss = resturentService.voteToResturents(model.getId(), resturentModel);
                        if (ss) {
                            return new ResponseEntity(new ResponseModel("200", "Voted successfully"), HttpStatus.OK);
                        }
                        return new ResponseEntity(new ResponseModel("404", "Vote not successfull"), HttpStatus.NOT_FOUND);
                    }
                    return new ResponseEntity(new ResponseModel("409", "User already voted for this resturent"), HttpStatus.CONFLICT);
                }
                return new ResponseEntity(new ResponseModel("400", error), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity(new ResponseModel("401", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error voting in resturent " + e.getMessage());
            return new ResponseEntity<>(new ResponseModel("500", "Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
