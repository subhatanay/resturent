/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.utils;

import com.resturent.petpuja.models.ResturentModel;
import com.resturent.petpuja.models.SignUpModel;

/**
 *
 * @author subhajgh
 */
public class Validator {

    public static String validateSignUpModel(SignUpModel signModel, String type) {
        if (signModel == null) {
            return "Please provide a proper sign up payload";
        }
        if (signModel != null) {
            if (signModel.getUserId() == null || "".equals(signModel.getUserId().trim())) {
                return "User id should not be empty";
            }
            if (signModel.getPassword() == null || "".equals(signModel.getPassword().trim())) {
                return "Password should not be empty";
            }
            if (type == "signup") {
                if (signModel.getName() == null || "".equals(signModel.getName().trim())) {
                    return "Name should not be empty";
                }

                if (signModel.getEmailAddress() == null || "".equals(signModel.getEmailAddress().trim())) {
                    return "Email address should not be empty";
                }
            }
        }
        return "";
    }
    
    public static String validateResturentModelForVote(ResturentModel res) {
        if (res == null) {
            return "Please provide a proper resturent payload";
        }
        if (res != null) {
            if (res.getResturentId() == null || "".equals(res.getResturentId().trim())) {
                return "Resturent Id should not be empty";
            }
            if (res.getRate() == null || "".equals(res.getRate().trim())) {
                return "Rate should not be empty";
            }
            try {
                float ff = Float.parseFloat(res.getRate());
                if (!(ff>=0.0 && ff<=5.0)) {
                    return "Rate should be between 0 to 5";
                }
            } catch (Exception e) {
                return "Rate should be integer or decimal";
            }
        }
        return "";
    }
}
