/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.resturent.petpuja.models.SignUpModel;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author subhajgh
 */
@Component
public class JWTGenerator {

    @Value("${key.secret}")
    static String jwtSecret;

    public String createJWTToken(SignUpModel signModel) {
        try {
      
            Algorithm algorithm = Algorithm.HMAC256("keysecret");
            long currentTime = new Date().getTime();
            String token = JWT.create()
                    .withIssuer("myowntoken")
                    .withClaim("name", signModel.getName())
                    .withClaim("emailAddress", signModel.getEmailAddress())
                    .withClaim("userId", signModel.getUserId())
                    .withClaim("id", signModel.getId())
                    .withIssuedAt(new Date())
                    .withExpiresAt(new Date(currentTime + 3600000))
                    .sign(algorithm);
            return token;
        } catch (JWTCreationException exception) {
            System.out.println(exception.getMessage());
        }
        return "";
    }

    public SignUpModel verifyJWTToken(String jwtToken) throws IOException {
        try {
            Algorithm algorithm = Algorithm.HMAC256("keysecret");
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("myowntoken")
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(jwtToken);
            ObjectMapper mapper = new ObjectMapper();
            
            String jsonPayload = new String(Base64.getDecoder().decode(jwt.getPayload()));
             
            SignUpModel model = mapper.readValue(jsonPayload, SignUpModel.class);
            return model;
        } catch (JWTVerificationException exception) {
            System.out.println(exception.getMessage());
        }
        return null;
    }

}
