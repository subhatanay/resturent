/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.dao;

import com.resturent.petpuja.models.SignUpModel;
import com.resturent.petpuja.utils.PasswordHash;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author subhajgh
 */
@Repository
public class AuthenticationDaoImpl implements AuthenticationDao {

    @Autowired
    JdbcTemplate jdbcTemplate;
    Logger logger = LoggerFactory.getLogger(AuthenticationDaoImpl.class);
    @Override
    public boolean createUser(SignUpModel signUp) {
        try {
            String sqlString = "INSERT INTO user_details (userid,password,name,email_address) VALUES (?,?,?,?)";
            List<Object> params = new ArrayList<>();
            params.add(signUp.getUserId());
            params.add(PasswordHash.hashPassword(signUp.getPassword()));
            params.add(signUp.getName());
            params.add(signUp.getEmailAddress());

            return jdbcTemplate.update(sqlString, params.toArray()) > 0;
        } catch (DuplicateKeyException ex) {
            return false;
        }
    }

    @Override
    public boolean verifyUser(SignUpModel signUp) {
        try {
            
            String textPassword = signUp.getPassword();
            logger.info("password checked " + textPassword );
            String sqlString = "SELECT password,name,email_address,id from user_details where userid=?";
            List<Object> params = new ArrayList<>();
            params.add(signUp.getUserId());
            jdbcTemplate.query(sqlString, params.toArray(), (rs) -> {
                signUp.setName(rs.getString("name"));
                signUp.setEmailAddress(rs.getString("email_address"));
                signUp.setPassword(rs.getString("password"));
                signUp.setId(rs.getString("id"));
            });
            logger.info("password checked " + signUp.getPassword());
            return (PasswordHash.verifyPassword(signUp.getPassword(), textPassword)) == true;

        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

}
