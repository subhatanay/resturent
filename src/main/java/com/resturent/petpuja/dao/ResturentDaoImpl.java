/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.dao;

import com.resturent.petpuja.models.Filter;
import com.resturent.petpuja.models.ResturentModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author subhajgh
 */
@Repository
public class ResturentDaoImpl implements ResturentDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    Logger logger = LoggerFactory.getLogger(ResturentDaoImpl.class);

    @Override
    public List<ResturentModel> findResturents(Filter filter) {
        StringBuilder sqlString = new StringBuilder("SELECT * from resturent_details ");
        List<Object> obj = new ArrayList<>();
        if (filter != null) {
            if (filter.getFilterSQL() != null) {
                sqlString.append(" where ").append(filter.getFilterSQL());
            }
            if (filter.getOrderBySQL() != null) {
                sqlString.append(" order by ").append(filter.getOrderBySQL());
            }
            sqlString.append(" limit ?,? ");
            obj.add(filter.getOffset());
            obj.add(filter.getLimit());

        } else {
            sqlString.append(sqlString);
        }
        logger.info("Search SQL :: " + sqlString);
        List<ResturentModel> resturentInfos = new ArrayList<>();

        List<Map<String, Object>> results = jdbcTemplate.queryForList(sqlString.toString(), obj.toArray());
        results.stream().map(rows -> {
            ResturentModel info = new ResturentModel();
            info.setResturentName((String) rows.get("resturent_name"));
            info.setCuisines((String) rows.get("cuisines"));
            info.setCurrency((String) rows.get("currency"));
            info.setCost(String.valueOf(rows.get("cost")));
            info.setResturentId(String.valueOf(rows.get("id")));
            info.setHasTable(String.valueOf(rows.get("hastable")));
            info.setHasOnline(String.valueOf(rows.get("hasonline")));
            info.setRate(String.valueOf(rows.get("rate")));
            info.setVote(String.valueOf(rows.get("votes")));
            return info;
        }).forEach(cc -> {
            resturentInfos.add(cc);
        });
        return resturentInfos;
    }

    @Override
    public long totalChannels(Filter filter) {
        StringBuilder sqlString = new StringBuilder("SELECT COUNT(*) from resturent_details ");
        if (filter != null && filter.getFilterSQL() != null) {
            sqlString.append(" where " + filter.getFilterSQL());
        }
        return jdbcTemplate.queryForObject(sqlString.toString(), Long.class);
    }

    @Override
    public boolean voteToResturent(String userid, ResturentModel resturentModel) {
        try {
            String aggreatedRate_VoteSQL = "select rate,votes from resturent_details where id=?";
            logger.info("Querying for resturents details .....");
            List<String> results = new ArrayList<>();
            jdbcTemplate.query(aggreatedRate_VoteSQL, new Object[]{resturentModel.getResturentId()}, (rs) -> {
                results.add(String.valueOf(rs.getString("rate")));
                results.add(String.valueOf(rs.getInt("votes")));
            });

            if (results.size() > 0) {
                logger.info("Getting proper resturent ...");
                String newVotedContened = "select resturent_id,AVG(rate) avg_rate,COUNT(resturent_id) count_vote from user_rating where resturent_id=? group by resturent_id";
                jdbcTemplate.query(newVotedContened, new Object[]{resturentModel.getResturentId()}, (rs) -> {
                    results.add(String.valueOf(rs.getString("avg_rate")));
                    results.add(String.valueOf(rs.getInt("count_vote")));
                });
                logger.info("Fetching newly users rating done ... calculating and adding the avg rating ....");
                String sqlString = "INSERT INTO user_rating VALUES(?,?,?)";
                List<Object> params = new ArrayList<>();
                params.add(userid);
                params.add(resturentModel.getResturentId());
                params.add(resturentModel.getRate());

                if (jdbcTemplate.update(sqlString, params.toArray()) > 0) {
                    logger.info("Insert done.... updateting avg rate and vote to prev table");
                    String updateAggretaterating = "update resturent_details SET votes = votes+1 , rate=? where id=?";
                    float avg_rate = ((Float.valueOf(results.get(0)) * Float.valueOf(results.get(1))) / (Float.valueOf(results.get(1) + 1))) * 10;
                    logger.info("Calculated new rate :: " + avg_rate);
                    params = new ArrayList<>();
                    params.add(String.valueOf(avg_rate));
                    params.add(resturentModel.getResturentId());
                    jdbcTemplate.update(updateAggretaterating, params.toArray());
                    logger.info("Vote updated successfully");
                    return true;
                }
                logger.info("Insert not done properly.. vote not updated");
                return false;
            }
            logger.info("Resturents not found");
            return false;
        } catch (EmptyResultDataAccessException ex) {

        }
        return false;
    }

    @Override
    public boolean isUserAlreadyVoted(String userid, ResturentModel resturentModel) {
        String querySQL = "select COUNT(*) from user_rating  where user_id=? and resturent_id=?";
        return jdbcTemplate.queryForObject(querySQL, new Object[]{userid, resturentModel.getResturentId()}, Integer.class) > 0;
    }

    @Override
    public List<String> listOfVotedResturents(String userid) {
        String querySQL = "select resturent_id from user_rating where user_id=?";
        List<Map<String, Object>> map_collection = jdbcTemplate.queryForList(querySQL, new Object[]{userid});
        List<String> listOfLikedResturents = new ArrayList<>();
        if (map_collection.size() > 0) {
            map_collection.stream().forEach((rs) -> {
                listOfLikedResturents.add(String.valueOf(rs.get("resturent_id")));
            });
            return listOfLikedResturents;
        }
        return null;
    } 
}
