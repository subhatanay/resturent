/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.dao;

import com.resturent.petpuja.models.Filter;
import com.resturent.petpuja.models.ResturentModel;
import com.resturent.petpuja.models.ResultModel;
import java.util.List;

/**
 *
 * @author subhajgh
 */
public interface ResturentDao {
    public  List<ResturentModel> findResturents(Filter filter);
    public long totalChannels(Filter filter);
    public boolean voteToResturent(String userid,ResturentModel resturentModel);
    public boolean isUserAlreadyVoted(String userid,ResturentModel resturentModel);
    public List<String> listOfVotedResturents(String userid);
}
