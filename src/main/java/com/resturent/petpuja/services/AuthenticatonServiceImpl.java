/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.services;

import com.resturent.petpuja.dao.AuthenticationDao;
import com.resturent.petpuja.models.SignUpModel;
import com.resturent.petpuja.utils.JWTGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author subhajgh
 */
@Service
public class AuthenticatonServiceImpl implements AuthenticationService {
    @Autowired
    AuthenticationDao authenticationDao; 
    @Autowired
    JWTGenerator generator;
    
    @Override
    public boolean createUser(SignUpModel signUp) {
        return authenticationDao.createUser(signUp);
    }

    @Override
    public String verifyUser(SignUpModel signUp) {
        if (authenticationDao.verifyUser(signUp)) {
            String token = generator.createJWTToken(signUp);
            return token;
        }
        return "";
    }
    
}
