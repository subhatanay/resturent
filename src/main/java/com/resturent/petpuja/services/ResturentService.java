/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.services;

import com.resturent.petpuja.models.ResturentModel;
import com.resturent.petpuja.models.ResultModel;

/**
 *
 * @author subhajgh
 */
public interface ResturentService {
    public  ResultModel findResturents(String filter,int limit,int offset,String sortOrder);
    public boolean voteToResturents(String userid,ResturentModel rest);
     public boolean isUserAlreadyVoted(String userid,ResturentModel resturentModel);
}
