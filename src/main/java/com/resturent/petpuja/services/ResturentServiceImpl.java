/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.services;

import com.resturent.petpuja.dao.ResturentDao;
import com.resturent.petpuja.models.Filter;
import com.resturent.petpuja.models.MetaData;
import com.resturent.petpuja.models.ResturentModel;
import com.resturent.petpuja.models.ResultModel;
import com.resturent.petpuja.utils.FilterUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author subhajgh
 */
@Service
public class ResturentServiceImpl implements ResturentService {
    @Autowired
    ResturentDao resturentDao;
    
    @Override
    public ResultModel findResturents(String filter, int limit, int offset, String sortOrder) {
        Filter filterContent = FilterUtils.parseFilter(filter, limit, offset, sortOrder);
        ResultModel resultInfo = new ResultModel();

        if (filterContent.getErrorText() == null) {
            MetaData meta = new MetaData();
            meta.setTotal(resturentDao.totalChannels(filterContent));
            if (offset > meta.getTotal()) {
                offset = (int) meta.getTotal();
            } else if (offset<=0) {
                offset=0;
            }
            List<ResturentModel> channelInfos = resturentDao.findResturents(filterContent);

            meta.setLimit(limit);
            meta.setOffset(offset);
            resultInfo.setMetaInfo(meta);
            resultInfo.setResults(channelInfos);

        } else {
            resultInfo.setError(filterContent.getErrorText());
        }

        return resultInfo;
    }

    @Override
    public boolean voteToResturents(String userid, ResturentModel rest) {
        
        return resturentDao.voteToResturent(userid, rest);
    }

    @Override
    public boolean isUserAlreadyVoted(String userid, ResturentModel resturentModel) {
        return resturentDao.isUserAlreadyVoted(userid, resturentModel);
    }

     
    
}
