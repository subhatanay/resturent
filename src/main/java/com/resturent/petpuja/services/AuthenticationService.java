/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.services;

import com.resturent.petpuja.models.SignUpModel;

/**
 *
 * @author subhajgh
 */
public interface AuthenticationService {
    public abstract boolean createUser(SignUpModel signUp);
    public abstract String verifyUser(SignUpModel signUp);
}
