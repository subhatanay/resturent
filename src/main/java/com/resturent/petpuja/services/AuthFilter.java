/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.resturent.petpuja.services;

import com.resturent.petpuja.models.SignUpModel;
import com.resturent.petpuja.utils.JWTGenerator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author subhajgh
 */
@Component
public class AuthFilter implements Filter {

    @Autowired
    AuthenticationService authServiceDao;
    @Autowired
    JWTGenerator generator;

    public AuthFilter() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String method = req.getMethod();

        if (method.equals("OPTIONS")) {
            res.setStatus(200);
            return;
        }
        String authorization = req.getHeader("Authorization");

        //check user sent the creds or not
        boolean unAuthRequest = checkRequestedUrlUnthenticated(req, getUnAuthenticatedAPIs());

        if (unAuthRequest == true) {
            chain.doFilter(request, response);
            return;
        }

        if (authorization == null) {
            // check creds present in cookie or not
            authorization = "";
            if ("".equals(authorization)) {
//                res.setHeader("WWW-Authenticate", "Basic");
                res.sendError(401, "UnAuthorized");
                return;
            }
        }
         
        authorization = authorization.replace("JWT ", "");

        SignUpModel model = generator.verifyJWTToken(authorization);
        //check user have valid user creds
        if (!checkRequestedUrlUnthenticated(req, getUnAuthenticatedAPIs())) {
            if (model != null && model.getId() != null) {
                request.getServletContext().setAttribute("authModel", model);
                chain.doFilter(request, response);
            } else {
                res.sendError(401, "UnAuthorized");
                return;
            }
        } else {
            chain.doFilter(request, response);
        }

    }

    private List<String> getUnAuthenticatedAPIs() {
        List<String> apis = new ArrayList<>();
        apis.add("GET_/favicon.ico");
        apis.add("GET_/login_ui");
        apis.add("GET_/home");
        apis.add("POST_/login");
        apis.add("GET_/resturents");
        apis.add("POST_/signup");
        return apis;
    }

    private boolean checkRequestedUrlUnthenticated(HttpServletRequest req, List<String> apis) {
        String requestedURL = req.getRequestURI();
        String method = req.getMethod();
        if (method == "OPTIONS") {
            return true;
        }
        String mm = method + "_" + requestedURL;
        if (apis.contains(mm)) {
            return true;
        } else if (requestedURL.contains("/libs/")) {
            return true;
        } else {
            return false;

        }
    }

    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {

    }

}
